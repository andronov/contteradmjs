'use strict';

/**
 * @ngdoc service
 * @name contterApp.feed
 * @description
 * # feed
 * Service in the contterApp.
 */
angular.module('contterApp')
  .service('admin', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  }).
  factory('Admin', ['$http', function ($http) {
    console.log('factory');

    var Admin = {
      test: test,
      get_users: get_users,
      get_sites: get_sites,
      create_sites: create_sites,
      update_sites: update_sites,
    };

    return Admin;

    ////////////////////
    function test() {
      return $http.post('/api/v1/admin/test/');
    }

    function get_users() {
      return $http.post('/api/v1/admin/get/users/', {});
    }

    function get_sites() {
      return $http.post('/api/v1/admin/get/sites/', {});
    }

    function create_sites() {
      return $http.post('/api/v1/admin/create/sites/', {});
    }

    function update_sites(data) {
      return $http.post('/api/v1/admin/update/sites/', data);
    }

  }]);
