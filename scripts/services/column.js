'use strict';

/**
 * @ngdoc service
 * @name contterApp.feed
 * @description
 * # feed
 * Service in the contterApp.
 */
angular.module('contterApp')
  .service('column', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  }).
  factory('Column', ['$http', function ($http) {
    console.log('factory');

    var Column = {
      all: all,
      get: get,
      top: top,
      create: create,
      create_trend: create_trend,
      settings_word: settings_word,
      settings_sort: settings_sort,
      settings_size: settings_size,
      settings_delete: settings_delete,
      settings_realtime: settings_realtime,
      settings_w_walls: settings_w_walls,
      settings_w_source: settings_w_source,
      get_settings_w_source: get_settings_w_source,
      get_settings_w_walls: get_settings_w_walls,
      recontent: recontent,
      favourite: favourite,
      get_favourite: get_favourite,
      related_create: related_create,
      get_recontent: get_recontent,
      get_related: get_related,
      preloader: preloader,
      get_letters: get_letters,
      toggle_trend: toggle_trend,
      name_trend: name_trend,
      name_top: name_top,
      create_top: create_top,
      toggle_top: toggle_top
    };

    return Column;

    ////////////////////

    /**
     * @name all
     * @desc Get all Posts
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function all() {
      return $http.get('/api/v1/wall/');
    }


    /**
     * @name create
     * @desc Create a new Post
     * @param {string} content The content of the new Post
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function create(data) {
      return $http.post('/api/v1/column/create/', data);
    }

    
    function settings_realtime(column, action) {
      return $http.post('/api/v1/column/settings/realtime/', {
        column: column,
        action: action
      });
    }

    function settings_size(column, size) {
      return $http.post('/api/v1/column/settings/size/', {
        column: column,
        size: size
      });
    }

    function settings_sort(sorter) {
      return $http.post('/api/v1/column/settings/sort/', {
        sorter: sorter
      });
    }

    function settings_delete(column_id) {
      return $http.post('/api/v1/column/settings/delete/', {
        column_id: column_id
      });
    }

    function settings_word(column, words, action, types) {
      return $http.post('/api/v1/column/settings/words/', {
        column: column,
        action: action,
        words: words,
        types: types
      });
    }

    

    function get_settings_w_source() {
      return $http.post('/api/v1/column/settings/get/w/source/',{
      });
    }
    function get_settings_w_walls() {
      return $http.post('/api/v1/column/settings/get/w/walls/',{
      });
    }
    function settings_w_walls(action, column, id, current_id) {
      return $http.post('/api/v1/column/settings/edit/w/walls/',{
        action: action,
        column: column,
        id: id,
        current_id: current_id
      });
    }

    function settings_w_source(action, column, id) {
      return $http.post('/api/v1/column/settings/edit/w/source/', {
        action: action,
        column: column,
        id: id
      });
    }

    function favourite(column, id, action) {
      return $http.post('/api/v1/column/favourite/', {
        column: column,
        action: action,
        id: id
      });
    }

    function get_favourite(column) {
      return $http.post('/api/v1/column/get/favourite/', {
        column: column
      });
    }

    function recontent(column, id, action) {
      return $http.post('/api/v1/column/recontent/', {
        column: column,
        action: action,
        id: id
      });
    }

    function top(wall_id) {
      return $http.post('/api/v1/column/top/', {
        wall_id: wall_id
      });
    }

    function preloader(column, page, ids, action, userType) {
      return $http.post('/api/v1/column/preloader/', {
        column: column,
        action: action,
        page: page,
        ids: ids,
        userType: userType
      });
    }

    function get_recontent(column) {
      return $http.post('/api/v1/column/get/recontent/', {
        column: column
      });
    }

    function get_related(column, id) {
      return $http.post('/api/v1/column/get/related/', {
        column: column,
        id: id
      });
    }

    function related_create(column, id, size) {
      return $http.post('/api/v1/column/create/related/', {
        column: column,
        id: id,
        size: size
      });
    }

    function get_letters(letter, types) {
      return $http.post('/api/v1/column/get/letters/', {
        letter: letter,
        types: types
      });
    }

    function create_trend(data) {
      return $http.post('/api/v1/admin/create/column/trend/', data);
    }

    function create_top(data) {
      return $http.post('/api/v1/admin/create/column/top/', data);
    }

    function toggle_trend(action, column_id) {
      return $http.post('/api/v1/admin/toggle/column/trend/', {
        action: action,
        column_id: column_id
      });
    }

    function toggle_top(action, column_id) {
      return $http.post('/api/v1/admin/toggle/column/top/', {
        action: action,
        column_id: column_id
      });
    }

    function name_trend(name, column_id) {
      return $http.post('/api/v1/admin/name/column/trend/', {
        name: name,
        column_id: column_id
      });
    }

    function name_top(name, column_id) {
      return $http.post('/api/v1/admin/name/column/top/', {
        name: name,
        column_id: column_id
      });
    }


    /**
     * @name get
     * @desc Get the Posts of a given user
     * @param {string} username The username to get Posts for
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function get(id) {
      return $http.get('/api/v1/wall/slug/');
      //return $http.get('/api/v1/wall/'+id+'/');
    }

  }]);
