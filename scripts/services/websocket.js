angular.module('contterApp')
  .factory('MySocket', function($websocket) {
      // Open a WebSocket connection
      var ws = $websocket('ws://contter.dev/api/v1/ws/test/');

      var w_columns = [];

      var ws_new_links = [];
      var ws_new_related_links = [];


      console.log(typeof ws_new_links);

      ws.onMessage(function(message) {
        var msg = JSON.parse(JSON.parse(message.data).msg);
        if(msg.cmd){
          console.log('=', msg);
        }
                
        switch(msg.cmd) {
          case "SELECT":

          break;
          case "INSERT":
              if(msg.name == 'ws_new_links'){
                ws_new_links.push(msg.data);
              };
              if(msg.name == 'ws_new_related_links'){
                ws_new_related_links.push(msg.data);
              };
          break;
          case "UPDATE":
          
          break;
          case "DELETE":

          break;
          case "CUSTOM":

          break;
          case "OTHER":

          break;
        }

        //ws_new_links.push(msg.data);
      });

    console.log('yesssss', ws_new_links);

    var methods = {
      w_columns: w_columns,
      ws_new_links: ws_new_links,
      ws_new_related_links: ws_new_related_links,
      status: function() {
        return ws.readyState;
      },
      /*noti_ws_new_links: function() {
        return ws.readyState;
      },*/
      send: function(message) {
        if (angular.isString(message)) {
          ws.send(message);
        }
        else if (angular.isObject(message)) {
          ws.send(JSON.stringify(message));
        }
      }
    };

    function sendHeartBeat() {
      ws.send("{}");
      setTimeout(sendHeartBeat, 5000);
    }

    ws.onError(function(event) {
      console.log('connection Error', event);
    });

    ws.onClose(function(event) {
      console.log('connection closed', event);
    });

    ws.onOpen(function() {
      sendHeartBeat();
      console.log('connection open');
    });

      return methods;
  })