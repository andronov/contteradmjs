'use strict';

/**
 * @ngdoc service
 * @name contterApp.feed
 * @description
 * # feed
 * Service in the contterApp.
 */
angular.module('contterApp')
  .service('wall', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  }).
  factory('Wall', ['$http', function ($http) {
    console.log('factory');

    var Wall = {
      all: all,
      get: get,
      create: create,
      filter: filter,
      sites: sites,
      trends: trends,
      top: top,
      walls: walls,
      category: category,
      list: list,
      settings: settings,
      welcome: welcome,
      toggle_wall: toggle_wall,
      create_categ_top: create_categ_top,
      create_categ_walls: create_categ_walls,
      toggle_cat_walls: toggle_cat_walls,
      create_wall: create_wall,
    };

    return Wall;

    ////////////////////
    function filter(username, slugwall) {
      return $http.post('/api/v1/wall/filter/', {
        username: username,
        slugwall: slugwall
      });
    }

    ////////////////////
    function sites(short_url) {
      return $http.post('/api/v1/wall/sites/', {
        short_url: short_url,
      });
    }

    ////////////////////
    function trends() {
      return $http.post('/api/v1/wall/trends/', {
      });
    }

    ////////////////////
    function top(cat, id) {
      return $http.post('/api/v1/wall/top/', {
        cat: cat,
        id: id
      });
    }

    ////////////////////
    function walls(cat, id) {
      return $http.post('/api/v1/wall/walls/', {
        cat: cat,
        id: id
      });
    }

    ////////////////////
    function category(types) {
      return $http.post('/api/v1/wall/category/?c=' +Math.random(), {
        types: types
      });
    }

    ////////////////////
    function list(column_id) {
      return $http.post('/api/v1/wall/list/?c=' +Math.random(), {
        column_id: column_id
      });
    }

    function toggle_wall(action, wall_id) {
      return $http.post('/api/v1/admin/toggle/wall/top/', {
        action: action,
        wall_id: wall_id
      });
    }

    function toggle_cat_walls(action, wall_id) {
      return $http.post('/api/v1/admin/toggle/category/walls/', {
        action: action,
        wall_id: wall_id
      });
    }

    function create_categ_top(name) {
      return $http.post('/api/v1/admin/create/wall/top/', {
        name: name
      });
    }

    function create_categ_walls(name) {
      return $http.post('/api/v1/admin/create/category/walls/', {
        name: name
      });
    }

    function create_wall(data) {
        console.log('create_wall base', data);
        var formData = new FormData();
        for(var d in data) {
          formData.append(d, data[d]);
        };
        return $http({method: 'POST', url: '/api/v1/admin/create/wall/walls/',
                         data: formData,
                         headers: {'Content-Type': undefined},
                         transformRequest: angular.identity})
    }
    

    

    /**
     * @name all
     * @desc Get all Posts
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function all() {
      return $http.get('/api/v1/wall/');
    }

    


    /**
     * @name create
     * @desc Create a new Post
     * @param {string} content The content of the new Post
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function create(name, color, sort) {
      return $http.post('/api/v1/wall/create/', {
        name: name,
        color: color,
        sort: sort
      });
    }

    function settings(types, wall_id, value) {
      return $http.post('/api/v1/wall/settings/', {
        types: types,
        wall_id: wall_id,
        value: value
      });
    }

    function welcome(walls) {
      return $http.post('/api/v1/wall/welcome/', {
        walls: walls
      });
    }


    /**
     * @name get
     * @desc Get the Posts of a given user
     * @param {string} username The username to get Posts for
     * @returns {Promise}
     * @memberOf thinkster.posts.services.Posts
     */
    function get(id) {
      return $http.get('/api/v1/wall/slug/?id='+id);
      //return $http.get('/api/v1/wall/'+id+'/');
    }

  }]);
