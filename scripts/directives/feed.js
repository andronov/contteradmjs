'use strict';

/**
 * @ngdoc directive
 * @name contterApp.directive:feed
 * @description
 * # feed
 */
angular.module('contterApp')
  .directive('feed', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the feed directive');
      }
    };
  })
  .directive('ccolumn', function ($timeout) {
    var directive = {
      /*controller: 'FeedCtrl',
      controllerAs: 'vm',*/
      restrict: 'AE',
      link: function(scope, element, attrs) {
       /*$timeout(function () {
          console.log('column', $(element));
          console.log('scope', scope);
          var id = $(element).data('column');

          $(element).find('.acs-size').click(function(){
            scope.settingsEditSizeColumn({el:id, em: 3})
          });

        });*/
      },
      scope: {
        columns: '=',
        profiles: '=',
        showMenuColumn: '&',
        hoverColumnActive: '&',
        settingsEditSizeColumn: '&',
        settingsEditPlusWordColumn: '&',
        settingsEditSourceWColumn: '&',
      },
      templateUrl: '/views/feed/column.html?s='+ Math.random()
    };
    return directive;

  })
  .directive('keyEnter', function (toastr) { 
    return function (scope, element, attrs) {
        var input = $(element);
        var parent = input.parents('.add-source-group-block');
        var parent_dop = parent.find('.add-words-group-block');
        var buffer = input.siblings('.input-buffer-word');
        var add_button = parent.find('.add-words-icon');

        element.bind("keyup paste", function (event) {
          event.preventDefault();
          var valu = input.val();
          var words = [];
          var next = true;
          var value = valu.replace(/[^A-Za-z А-Яа-я ñÑ]/g, '');
          buffer.text(value);

          parent_dop.find(".add-source-group-block-word").each(function(indx, element){
            if(!$(element).hasClass('dop')){words.push($(element).find('span').html().toLowerCase())}
          });

          if(words.indexOf(value) != -1){
            next = false;
          }

          if(event.which == 13 && value.length>=3 && next) {
              add_button.click();
              return false
          }
          

          var dop = parent.find('.add-source-group-block-word.dop');
          if(dop.length==0 && value.length>0){
            parent_dop.prepend('<div  class="acs-load add-source-group-block-word dop"><span>'+value+'</span> <i class="icon-cancel icon-dops"></i></div>');
          }
          else if(value.length==0){
            dop.remove();
          }
          else{
            dop.find('span').html(value);
          }

          parent_dop.find('.add-source-group-block-word i.icon-dops').unbind();
          parent_dop.find('.add-source-group-block-word i.icon-dops').click(function(e){
            scope.settingsEditWordsWColumn(e, 'delete');
          });

          var right_add = '0px';
          if(value.length<3 || !next){
            right_add = '-29px';
          }
          else{
            
          }

          add_button.animate(
                  {'right': right_add}, {
                  easing: null,
                  duration: 200,
                  complete: function() {
                  }
          });
          

        });
    };
  })
  .directive('scrollColumn', function ($timeout) { 
    var directive = {
      link: function(scope, element, attrs) {

        var prt = $(element).parents('.column');
        var r = $(prt).find('.column-panel.column-rel-panel');
        var st = $(prt).find('.column-panel.column-standart-panel');


        if(!prt.hasClass('rel-column')){

        $(element).find('.column-content').scroll(function(e){

          //$(prt).find('.image-blur').blurryLoad();

            if(($(this).prop('scrollHeight') == ($(this).scrollTop() + $(this).height())) && !$(this).hasClass('pre-loader')){
              $(this).addClass('pre-loader');
              
              var preloader = '<div class="pre-loader-after-links"><div class="loader  loader-ellipsis-pre" data-type="default"></div></div>';
              $(this).find('.column-container').append(preloader);
              var userType = scope.$parent.$parent.userType;
              if (userType == 'Current'){
                scope.$parent.$parent.columnPreloader($(element), prt.data('column'), 'standart');
              }
              else if(userType == 'Anonymus'){
                scope.$parent.$parent.columnPreloaderAnonymus($(element), prt.data('column'), 'standart');
              }
              else if(userType == 'Alien'){
                scope.$parent.$parent.columnPreloaderAlien($(element), prt.data('column'), 'standart');
              }
            }

            /*var s = $(this).scrollTop();
            if(s==0 && !prt.hasClass('rel-column')){
              //r.show();
              //get recontent

              r.removeClass('yes__noti');
              r.addClass('non-scroll');
              st.addClass('non-scroll');
              r.find('.column-content').scrollTop(5000);///margin-bottom: 35px;
              r.css({'margin-bottom': '0px'});
              r.find('.column-content').css({'margin-top': '-60px'});
              //r.animate({'height': '100%'}, 1500, )
              r.animate(
                  {'height': '100%'}, {
                  easing: null,
                  duration: 1500,
                  complete: function() {
                    //r.css({'margin-bottom': '35px'})
                    
                    //r.animate({'margin-bottom': '35px'}, 1000);r.find('.column-content').animate({'margin-top': '-35px'}, 1000);
                    //r.find('.column-content').animate({'margin-top': '-35px'}, 1000);
                    r.find('.column-content').css({'margin-top': '-35px'});
                    r.css({'margin-bottom': '35px'});
                    r.removeClass('non-scroll');
                    st.removeClass('non-scroll');
                    scope.$parent.$parent.getReContent(prt.data('column'));
                  }
              });
              

            }*/
        });
        }


      },
    };
    return directive
  })   
  .directive('scrollReColumn', function ($timeout) { 
    var directive = {
      link: function(scope, element, attrs) {
        var prt = $(element).parents('.column');
        var st = $(prt).find('.column-panel.column-standart-panel');
        var r = $(prt).find('.column-panel.column-rel-panel');
        var count = 0;
      


         /*notify*/
        scope.$watch('notify', function(newValue, oldValue) {
          if (newValue){
            var id = prt.data('site');
            for (var key in newValue){
              console.log('countdfd', id,newValue[key]);
              if(newValue[key].site_id == id){count += 1}
            }
            if(count>0){
              $(element).addClass('yes__noti');
            }
            else{
              $(element).removeClass('yes__noti')
            }
            
          }
        }, true);        
        /* end notify */

        $(element).find('.column-content').scroll(function(e){
           var scrollBottom = $(this).scrollTop() + $(this).height();
            if(!r.hasClass('non-scroll') &&scrollBottom==$(this).prop('scrollHeight')){


              r.removeClass('yes__noti');
              r.addClass('non-scroll');
              st.addClass('non-scroll');
              r.find('.column-content').scrollTop(5000);
              //r.animate({'height': '0%'}, 1500);r.find('.column-content').scrollTop(5000);
              function f(){ r.find('.column-content').scrollTop(5000) };
              r.find('.column-content').css({'margin-top': '-60px'});


              var IntervalID = setInterval(f, 10);


              r.css({'margin-bottom': '0px'});
              r.animate(
                  {'height': '0%'}, {
                  easing: null,
                  duration: 1500,
                  complete: function() {
                    //r.css({'margin-bottom': '35px'});
                    //r.css({'margin-bottom': '60px'});
                    r.find('.column-content').css({'margin-top': '-60px'});
                    //r.find('.column-content').css({'margin-top': '-35px'});
                    r.css({'margin-bottom': '60px'});
                    r.removeClass('non-scroll');
                    st.removeClass('non-scroll');
                    clearInterval(IntervalID);
                    r.find('.column-content').scrollTop(0);
                  }
              });
              
              /*$timeout(function () {
                r.hide();
              }, 1800);*/
              
            }
        });
      },
      scope: {
        notify: '='
      },
    };
    return directive
  })    
  .directive('mouseTop', function ($timeout) { 
    var directive = {
      link: function(scope, element, attrs) {

        function addOnWheel(elem, handler) {
            if ('onwheel' in document) {
              // IE9+, FF17+
              elem.addEventListener("wheel", handler);
            } else if ('onmousewheel' in document) {
              // устаревший вариант события
              elem.addEventListener("mousewheel", handler);
            } else {
              // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
              elem.addEventListener("MozMousePixelScroll", handler);
            }
        }
    
        var scale = 1;
        var elem = document.getElementsByClassName('mouse-top')[0];
        var l = 0

        addOnWheel(elem, function(e) {
          
          var delta = e.deltaY || e.detail || e.wheelDelta;
          console.log('eeee', delta);
          if(delta == -100){
              l -= 100;
          }
          else{
            l += 100;
          }
          

          $('.column-content').animate({ // column-container-scroller //column-container
                scrollTop: l
          }, 500);

          e.preventDefault();
        });

       $timeout(function () {
          console.log($(element));
          /*$(element).mousewheel(function(e){
            console.log($(element));
          });*/
        });
      },
    };
    return directive
  })  

  .directive('mouseLeft', function ($timeout) { 
    var directive = {
      link: function(scope, element, attrs) {

        function addOnWheel(elem, handler) {
            if ('onwheel' in document) {
              // IE9+, FF17+
              elem.addEventListener("wheel", handler);
            } else if ('onmousewheel' in document) {
              // устаревший вариант события
              elem.addEventListener("mousewheel", handler);
            } else {
              // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
              elem.addEventListener("MozMousePixelScroll", handler);
            }
        }
    
        var scale = 1;
        var elem = document.getElementsByClassName('mouse-left')[0];
        var l = $('.block-container-columns').scrollLeft();

        addOnWheel(elem, function(e) {
          
          var delta = e.deltaY || e.detail || e.wheelDelta;
          console.log('eeee', delta, l);
          if(delta == -100){
              l -= 100;
          }
          else{
            l += 100;
          }
          
          
          $('.block-container-columns').animate({ // column-container-scroller //column-container
                scrollLeft: l
          }, 500);

          e.preventDefault();
        });

       $timeout(function () {
          console.log($(element));
          
        });
      },
    };
    return directive
  })  
  .directive('posts', function () {
    var directive = {
      restrict: 'A',
      scope: {
        posts: '='
      },

      templateUrl: '/views/feed/posts.html?s='+ Math.random()
    };
    return directive;

  })
  .directive('post', function ($timeout) {
    var directive = {
      restrict: 'A',
      /*link: function(scope, element, attrs) {
       $timeout(function () {

          $(element).find('.l-relevat').click(function(e){
            scope.$parent.$parent.$parent.$parent.$parent.Related(e, $(element).data('id'))
            //console.log(scope.$parent.$parent.$parent.$parent.$parent.ReContent(e, '1'));
          });

          $(element).find('.l-recontent').click(function(e){
            console.log(scope.$parent.$parent.$parent.$parent.$parent.ReContent(e, $(element).data('id')));
          });

        });
      },*/
      scope: {
        post: '='
      },
      templateUrl: '/views/feed/post.html?s='+ Math.random()
    };
    return directive;
  })
  .directive('notiColumn', function () {
    var directive = {
      link: function(scope, element, attrs) {
        var header = $(element).parents('.cntr-column-header');
        var block = $(element).find('.cntr-pallet-noti');
        var id = scope.column.id;
        var menu_block = $('.menu-item-column[data-column="'+id+'"] .column-notification');
        var count = 0;



        function showNoti(count){
          if(count>0){
            block.find('.cntr-pallet-noti-count').html(count);
            menu_block.find('.noti__text').html(count);
            block.addClass('active');
            menu_block.addClass('active');
            header.addClass('cntr-column-header-noti-yes')
          }
          else{
            header.removeClass('cntr-column-header-noti-yes')
          }
        };

        scope.$watch('notify', function(newValue, oldValue) {
          if (newValue){
            count = 0;
            for (var key in newValue){
              if(newValue[key].column_id == id){count += 1}
            }
            ///check realtime
            if(header.find('.acs_toggle_block').hasClass('checked-word')){
              scope.$parent.showNotificationColumn(scope.column);
            }
            else{
              showNoti(count)
            }
            
          }
        }, true);

        $(element).click(function(){
          block.removeClass('active');
          menu_block.removeClass('active');
          header.removeClass('cntr-column-header-noti-yes');
          scope.$parent.showNotificationColumn(scope.column);
        });
      },
      scope: {
        notify: '=',
        column: '=',
      },
      templateUrl: '/views/notification/n_column.html?s='+ Math.random()
    };
    return directive;

  })

  /**
   * Menu column
   * @param  {Object} )            {                                              var directive [description]
   * @param  {String} templateUrl: '/views/menu/menu_column.html?s [description]
   * @return {[type]}              [description]
   */
  .directive('menucolumn', function ($timeout) {
    var directive = {
      restrict: 'AE',
      link: function(scope, element, attrs) {
       $timeout(function () {
          $('.menu-item-column').unbind(); 
          $('.menu-item-column').click(function(){
            //scope.$parent.$parent.$parent.$parent.$parent.ReContent(e, $(element).data('id'));
            if(!$(this).hasClass('active')){
              var id = $(this).data('column');
              var column = $('.column[data-column="'+id+'"]');
              var l = column.offset().left;
              if(l< 50){l = 0};
              //$('.block-container-columns').scrollLeft(l);
              $('.block-container-columns').animate({
                scrollLeft: l
              }, 1000);
  
              console.log('click', column.offset().left);
  
              $('.menu-item-column').removeClass('active');
              $(this).addClass('active')
            }

          });
        }, 2000);
      },
      scope: {
        columns: '=',
        addColumn: '&'
      },
      templateUrl: '/views/menu/menu_column.html?s='+ Math.random()
    };
    return directive;

  })
  /**
   * Menu
   * @param  {Object} )            {                                             var directive [description]
   * @param  {String} templateUrl: '/views/menu/menu_walls.html?s [description]
   * @return {[type]}              [description]
   */
  .directive('menuwalls', function ($timeout) {
    var directive = {
      restrict: 'AE',
      link: function(scope, element, attrs) {
        //$($(element).find('.menu-item-wall').not('.menu-item-add-wall')).click(function(){ 
        $('.block-menu-list.block-menu-list-wall').on('click', '.menu-item-wall', function(e){  //
            scope.switchWall({el:e})
          });
        /*$timeout(function () {
          $($(element).find('.menu-item-wall').not('.menu-item-add-wall')).unbind();
          $($(element).find('.menu-item-wall').not('.menu-item-add-wall')).click(function(){
            scope.switchWall({el:$(this).data('wall')})
          });
        }, 2000);*/
      },
      scope: {
        walls: '=',
        switchWall: '&',
        addGroup: '&',
        addColumn: '&',
        showleftmenu: '=',

      },
      templateUrl: '/views/menu/menu_walls.html?s='+ Math.random(),

    };
    return directive;
  })
 /* .directive("switchWallsDer", function($timeout) {
    var directive = {
      scope: {
        switchWall: '&'
      },
      link: function(scope, element, attrs) {
        $(element).click(function(){
            console.log(scope);
            scope.switchWall({el:'edfl'})
        });
      }
    };
    return directive
  })*/
  /**
   * Menu add new column
   * @param  {Object} $timeout) {                                 var directive [description]
   * @param  {[type]} link:     function(scope, element, attrs) {                                 $(element).click(function(){            scope.addColumn()        });      }    };    return directive  } [description]
   * @return {[type]}           [description]
   */
  .directive("addColumnDer", function($timeout) {
    var directive = {
      scope: {
        addColumn: '&'
      },
      link: function(scope, element, attrs) {
        $(element).click(function(){
            scope.addColumn()
        });
      }
    };
    return directive
  })
  .directive("addGroupDer", function($timeout) {
    var directive = {
      scope: {
        addGroup: '&'
      },
      link: function(scope, element, attrs) {
        $(element).click(function(){
            scope.addGroup()
        });
      }
    };
    return directive
  })
  .directive("switchFooterMenu", function($timeout, $location) {
    var directive = {
      link: function(scope, element, attrs) {
        $(element).click(function(){
          var u = '/' + $(element).data('url');
          $location.path(u);
          scope.$apply();
        });
      }
    };
    return directive
  })
  .directive("settingscard", function($timeout) {
    var directive = {
      restrict: 'AE',
      link: function(scope, element, attrs) {
        console.log('test');
       /* $timeout(function () {
        $(element).find('.acs_toggle_block').click(function(){
          console.log('test2');
          var em = 'No';
          var es = false;
          if(element.hasClass('checked-site')){
            em = 'Yes';
            es = true;
            element.removeClass('checked-site').addClass('checked-word');
          }
          else{
            element.removeClass('checked-word').addClass('checked-site');
          }
        });
      }, 1000);*/
      },
      template: '<div class="ac-form-acs-public"><div class="acs_toggle_block  checked-word"><div class="acs_toggle_button"><div class="acs_toggle_site_text">No</div><div class="acs_toggle_word_text">Yes</div><span class="acs_toggle_handle"><span></span></span></div></div></div>'
    };
    return directive
  })
  .directive("addWallHtml", function($timeout) {
    var directive = {
      restrict: 'E',
      link: function(scope, element, attrs) {
        console.log('addwallhtml', scope);
      },
      scope: {
        addwalls: '=',
        addGroupClose: '=addGroupClose',
        addGroupSort: '=addGroupSort'
      },
      templateUrl: '/views/add-wall-popup.html?s='+ Math.random(),
    };
    return directive
  })
  .directive("addColumnHtml", function($timeout) {
    var directive = {
      restrict: 'E',
      link: function(scope, element, attrs) {
        console.log('addColumnHtml', scope);
        
      },
      scope: {
        addcolumns: '=',
        addcolumnsres: '=',
        letter: '=',
        AddColumnBackFeeds: '=addColumnBackFeeds',
        AShowRssFeeds: '=aShowRssFeeds',
        addColumnFromList: '=addColumnFromList',
      },
      templateUrl: '/views/add-column-list.html?s='+ Math.random(),
    };
    return directive
  })
  .directive('keyEnterAddColumn', function (toastr) { 
    return function (scope, element, attrs) {
        element.bind("keyup paste", function (event) {
          scope.AddColumnSearchWords(element.val())
        });
    };
  })
  .directive('imageBlurLoad', function ($timeout) { 
    return function (scope, element, attrs) {
        //console.log('.imageBlurLoad', scope, element, attrs);
        $timeout(function () {
          $(element).blurryLoad();
        });
    };
  })
  /*.directive("inputAddColumn", function($timeout) {
    var directive = {
      link: function(scope, element, attrs) {
        $(element).keyup(function(){
            console.log('inputAddColumn')
        });
      }
    };
    return directive
  })*/
  /*.directive('columnToggle', function () {
    var directive = {
      restrict: 'A',
      scope:{
        isOpen: "=columnToggle"
      },
      link: function(scope, element, attr) {
        console.log(scope, element, attr);
        scope.$watch('isOpen', function(newVal,oldVal){
            console.log('active');
      });
    }
  };
    return directive;
  })*/;
