'use strict';

/**
 * @ngdoc overview
 * @name contterApp
 * @description
 * # contterApp
 *
 * Main module of the application.
 */
angular
  .module('contterApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMessages',
    'ngWebSocket',
    'toastr',
    'ui.router',
    'satellizer',
    'angularLazyImg',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.pagination'
  ])  
  .config(function ($stateProvider, $urlRouterProvider, $authProvider, $locationProvider, $urlMatcherFactoryProvider) {
    $urlMatcherFactoryProvider.type('domainType', {}, function () {
      var GUID_REGEXP = /\b((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b/;

      return {
        encode: angular.identity,
        decode: angular.identity,
        is: function(item) {
          return GUID_REGEXP.test(item);
        }
      };
    })
    /*$routeProvider
      .when('/', {
        templateUrl: '/views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: '/views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/feed', {
        templateUrl: '/views/feed.html',
        controller: 'FeedCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });*/
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: '/views/main.html',
        controller: 'MainCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: '/views/about.html',
        controller: 'AboutCtrl'
      })
      .state('feed', {
        url: '/feed',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('feedd', {
        url: '/feed/',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('welcome', {
        url: '/welcome',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state('feeduserwall', {
        url: '/@:username/:slugwall',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        reloadOnSearch: false
        /*resolve: {
          loginRequired: loginRequired
        }*/
      })
      .state('feeduserwallcolumn', {
        url: '/@:username/:slugwall/:slugcolumn',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        reloadOnSearch: false
        /*resolve: {
          loginRequired: loginRequired
        }*/
      })
      .state('wall', {
        url: '/w/:slugwall',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        /*resolve: {
          loginRequired: loginRequired
        }*/
      })
      .state('new_digest', {
        url: '/new/digest',
        templateUrl: '/views/main.html',
        controller: 'MainCtrl'
      })
      .state('login', {
        url: '/login',
        templateUrl: '/views/login.html',
        controller: 'LoginCtrl',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: '/views/signup.html',
        controller: 'SignupCtrl',
        resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
      })
      .state('logout', {
        url: '/logout',
        template: null,
        controller: 'LogoutCtrl'
      })
      .state('user', {
        url: '/@:username',
        templateUrl: '/views/feed.html?s='+ Math.random(),
        controller: 'FeedCtrl',
        reloadOnSearch: false
        /*resolve: {
          loginRequired: loginRequired
        }*/
      })
      .state('profile', {
        url: '/profile',
        templateUrl: '/views/profile.html',
        controller: 'ProfileCtrl',
        resolve: {
          loginRequired: loginRequired
        }
      })
      .state("trends", {  
        url: "/trends/",
        templateUrl: '/views/trends.html',
        controller: 'TrendsCtrl'
      })
      .state("trendss", {  
        url: "/trends",
        templateUrl: '/views/trends.html',
        controller: 'TrendsCtrl'
      })
      .state("top", {  
        url: "/top/",
        templateUrl: '/views/top.html',
        controller: 'TopCtrl'
      })
      .state("topp", {  
        url: "/top",
        templateUrl: '/views/top.html',
        controller: 'TopCtrl'
      })
      .state("topcat", {  
        url: "/top/:category/",
        templateUrl: '/views/top.html',
        controller: 'TopCtrl'
      })
      .state("toppcat", {  
        url: "/top/:category",
        templateUrl: '/views/top.html',
        controller: 'TopCtrl'
      })
      .state("walls", {  
        url: "/walls/",
        templateUrl: '/views/walls.html',
        controller: 'WallsCtrl'
      })
      .state("wallss", {  
        url: "/walls",
        templateUrl: '/views/walls.html',
        controller: 'WallsCtrl'
      })
      .state("wallscat", {  
        url: "/walls/:category/",
        templateUrl: '/views/walls.html',
        controller: 'WallsCtrl'
      })
      .state("wallsscat", {  
        url: "/walls/:category",
        templateUrl: '/views/walls.html',
        controller: 'WallsCtrl'
      })
      .state("walldomain", {  
        url: "/{domain:domainType}",
        templateUrl: '/views/sites.html',
        controller: 'SitesCtrl'
      });

    $urlRouterProvider.otherwise('/');

    $authProvider.facebook({
      clientId: '657854390977827'
    });

    $authProvider.google({
      clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com'
    });

    $authProvider.github({
      clientId: '0ba2600b1dbdb756688b'
    });

    $authProvider.linkedin({
      clientId: '77cw786yignpzj'
    });

    $authProvider.instagram({
      clientId: '799d1f8ea0e44ac8b70e7f18fcacedd1'
    });

    $authProvider.yahoo({
      clientId: 'dj0yJmk9SDVkM2RhNWJSc2ZBJmQ9WVdrOWIzVlFRMWxzTXpZbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yYw--'
    });

    $authProvider.twitter({
      url: '/api/auth/twitter'
    });

    $authProvider.live({
      clientId: '000000004C12E68D'
    });

    $authProvider.twitch({
      clientId: 'qhc3lft06xipnmndydcr3wau939a20z'
    });

    $authProvider.oauth2({
      name: 'foursquare',
      url: '/auth/foursquare',
      clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
      redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
      authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
    });

    function skipIfLoggedIn($q, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.reject();
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    function loginRequired($q, $location, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/');
      }
      return deferred.promise;
    }
    $locationProvider.html5Mode(true);  
  }).config(['lazyImgConfigProvider', function(lazyImgConfigProvider){
    var scrollable = document.querySelector('.base-container');
    lazyImgConfigProvider.setOptions({
      offset: 100, // how early you want to load image (default = 100)
      errorClass: 'error', // in case of loading image failure what class should be added (default = null)
      successClass: 'success', // in case of loading image success what class should be added (default = null)
      onError: function(image){}, // function fired on loading error
      onSuccess: function(image){}, // function fired on loading success
      container: angular.element(scrollable) // if scrollable container is not $window then provide it here
    });
  }]);

$.fn.removeClassRegex = function(regex) {
  return $(this).removeClass(function(index, classes) {
    return classes.split(/\s+/).filter(function(c) {
      return regex.test(c);
    }).join(' ');
  });
};

$.fn.getClassRegex = function(regex) {
  var val = null
  $(this).removeClass(function(index, classes) {
    classes.split(/\s+/).filter(function(c) {
      if(regex.test(c)){
        console.log('c', c)
        val = c;
      }
    })
  });
  return val
};

function inViewport(element) {
    if (typeof jQuery === "function" && element instanceof jQuery) {
      element = element[0];
    }
    var elementBounds = element.getBoundingClientRect();
    return (
      elementBounds.top >= 0 &&
      elementBounds.left >= 0 &&
      elementBounds.bottom <= $(window).height() &&
      elementBounds.right <= $(window).width()
    );
  }


$.fn.blurryLoad = function (options) {

        var parentContainer = $(this).parent(),
            imageContainer = $(this),
            parentItem = $(this).parents('.column-item'),
            parentColumn = $(this).parents('.column'),
            p_top = parentItem.offset().top,
            p_left = parentItem.offset().left,
            w = $(window).width(),
            h = $(window).height(),
            scrl_top_clmn = parentColumn.find('.column-content').scrollTop();

        
        
        parentContainer.addClass('blurry-load-container');
        imageContainer.addClass('img-blur')

        // 1: load small image and show it
        var img = new Image();
        img.src = imageContainer.attr('src');
        img.onload = function () {
            imageContainer.addClass('loaded');
        };

       /* console.log('w', w, h, scrl_top_clmn);
        console.log('o', p_top,p_left,  parentItem.position());

        console.log('yesss', inViewport(parentItem))*/

        //if(h >= p_top){
          // 2: load large image
          var imgLarge = new Image();
          imgLarge.src = $(this).attr('data-large');
          imgLarge.onload = function () {
               imgLarge.classList.add('loaded');
               if(parentItem){
                parentItem.addClass('loaded')
               }
          };
          imgLarge.onerror = function () {
            imgLarge.classList.add('noloaded');
            if(parentItem){
              parentItem.addClass('noloaded')
            }
          };
          parentContainer.append(imgLarge)
        //}
       
    };