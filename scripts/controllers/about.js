'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('AboutCtrl', function ($scope, $stateParams) {
  	console.log('$stateParams', $stateParams);
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
