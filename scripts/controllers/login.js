angular.module('contterApp')
  .controller('LoginCtrl', function($scope, $location, $auth, $cookies, toastr, Account) {
    $scope.login = function() {
      $auth.login($scope.user)
        .then(function() {
          toastr.success('You have successfully signed in!');
          $location.path('/');
        })
        .catch(function(error) {
          toastr.error(error.data.message, error.status);
        });
    };
    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          $cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
          toastr.success('You have successfully signed in with ' + provider + '!');
         /* var getProfile = function() {
            Account.getProfile()
              .then(function(response) {
                console.log('Account', response.data);
                if(response.data.new){
                  $location.path('/welcome');
                }
                else{
                  $location.path('/feed');
                }
            })
            .catch(function(response) {
               toastr.error(response.data.message, response.status);
            });
          };
          getProfile(); */
          //$location.path('/feed');
        })
        .catch(function(error) {
          if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            toastr.error(error.error);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
    };
  });