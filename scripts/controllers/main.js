'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('MainCtrl', function ($scope, $location, $auth, $cookies, toastr, Account, Admin, uiGridConstants) {
  	console.log('main');

    $scope.menu = 'sites';

    $scope.users = {};

    $scope.users.columnDefs = [
      {name: "id", width: 50},
      {name: "username"},
      {name: "first_name"},
      {name: "last_name"},
      {name: "email"},
      {name: "type", width: 50},
      {name: "twitter"},
      {field: 'twitter_img', name: "twitter_img", width: 50, cellTemplate:"<img width=\"50px\" src=\"{{row.entity.twitter_img}}\">"},
      //{name: "description"},
      {field: 'avatar', name: "avatar", width: 50, cellTemplate:"<img width=\"50px\" src=\"{{row.entity.avatar}}\">"},
      {name: "new", width: 50},
      {name: "is_active", width: 50, enableSorting: false, enableHiding: false,  type: 'boolean' },
      {name: "created", width: 250, sort: {
          direction: uiGridConstants.DESC,
          priority: 1
        }},
    ];

    $scope.sites = {};

    $scope.sites.columnDefs = [
      {name: "id", width: 50, type: 'number'},
      {name: "name"},
      /*{name: "description"},*/
      {name: "url"},
      {name: "short_url"},
      {name: "rss"},
      {field: 'image', name: "image", width: 50, cellTemplate:"<img width=\"50px\" src=\"{{row.entity.image}}\">"},
      {field: 'favicon', name: "favicon", width: 50, cellTemplate:"<img width=\"16px\" src=\"{{row.entity.favicon}}\">"},
      //{name: "description"},
      {name: "color", width: 50},
      {name: "is_active", width: 45, enableSorting: false, enableHiding: false,  type: 'boolean' },
      {name: "created",  type: 'date',  width: 250, sort: {
          direction: uiGridConstants.DESC,
          priority: 1
        }},
    ];

    $scope.sites.onRegisterApi = function(gridApi){
          $scope.gridApi = gridApi;
          gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            console.log('edit', rowEntity);
            $scope.updateSites(rowEntity);
            $scope.$apply();
          });
        };

    $scope.updateSites = function(data) {
      Admin.update_sites(data).then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        console.log('updateSites', data.data);
      }
  
      function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }

      
    };  

    $scope.addDataSite = function() {
      var n = $scope.sites.data.length + 1;

      Admin.create_sites().then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        console.log('create_sites', data.data);
        $scope.sites.data.push(data.data.site);
      }
  
      function  fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }

      
    };


    function get_users(){
      Admin.get_users().then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        console.log('get_users', data.data);
        $scope.users.data = data.data.users;
        console.log('.$scope.users', $scope.users)
      }
  
      function  fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }
    };

    get_users();

    function get_sites(){
      Admin.get_sites().then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        console.log('get_sites', data.data);
        $scope.sites.data = data.data.sites;
      }
  
      function  fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }
    };

    get_sites();

    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          console.log('$auth', $auth.getToken()); 
          $cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
          toastr.success('You have successfully signed in with ' + provider + '!');
          var getProfile = function() {
            Account.getProfile()
              .then(function(response) {
                console.log('Account', response.data);
                if(response.data.new){
                  $location.path('/welcome');
                }
                else{
                  $location.path('/feed');
                  //$location.path('/@' + response.data.username);
                }
            })
            .catch(function(response) {
               toastr.error(response.data.message, response.status);
            });
          };
          getProfile(); 
          //$location.path('/feed');
        })
        .catch(function(error) {
          if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            toastr.error(error.error);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
    };

    ///SWITCH MEnu
    $scope.switchMenu = function($event){
      var el = $($event.currentTarget),
        type = el.data('type');

      $scope.menu = type;
      if(type == 'top'){
        $location.path("/top/");
      }
      else if(type == 'trends'){
        $location.path("/trends/");
      }
      else if(type == 'walls'){
        $location.path("/walls/");
      }
      
    }

  });
