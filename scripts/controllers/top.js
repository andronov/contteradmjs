'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:TopCtrl
 * @description
 * # TopCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('TopCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.removeClass('open-profile');

    if($scope.params.category){
      $scope.cat = $scope.params.category.toUpperCase();
    }
    else{
      $scope.cat = 'RECOMMENDED';
    }
    


    $scope.userType = 'Anonymus';

    $scope.top = true;
    $scope.cat_id = null;

    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params, $scope.cat);

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          $scope.slugusername = $scope.user.username;
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();



    var vm = this;


    $scope.posts = [];

    $scope.columns = [];

    $scope.categories = [];

    $scope.profiles = [];

    $scope.walls = [];

    activate();

    function get_columns() {

      Wall.top($scope.cat, $scope.cat_id).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('get_columnsget_columns', data);
          console.log(data.data.columns);

          $scope.userType = data.data.userType;
          $scope.columns = data.data.columns;
          //$scope.walls = data.data.walls;
      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    
    

    function activate() {

      Wall.category('is_top').then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('categorycategory', data);
          $scope.categories = data.data.categories;

          for(var i = 0; i < $scope.categories.length; i++){
            console.log('.$scope.categories', $scope.categories[i])
            if($scope.categories[i].name == $scope.cat){
              $scope.cat_id = $scope.categories[i].id
            }
          }
          if(($scope.params.category && $scope.cat_id) || !$scope.params.category){
            get_columns();
          }
          else{
            console.log('nononononono')
          }
          

      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    


    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

    };

    /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);

      if(!e.hasClass('active-menu-wl')){
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        $scope.$apply();
      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.topWall(wall)
      }
      
    };

    $scope.switchTopCat = function($event, cat){
      var el = $($event.currentTarget),
          prt = el.parent(),
          name = el.data('name'),
          url = "/top"
      
      if(el.hasClass('active')){
        return false
      }

      prt.find('.nav-menu-item-top').removeClass('active');
      el.addClass('active');

      if(name!='RECOMMENDED'){
        url += "/" + name.toLowerCase()
      }

      $location.path(url)
    };


    $scope.ShowProfile = function($event){
      $location.path('/@'+ $scope.user.username)
    };

    $scope.toggleTopColumn = function(categ){
      console.log("toggleTopColumn", categ);

      var action = true;
      if(categ.is_active){
        action = false
      }

      Wall.toggle_wall(action, categ.id).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
        console.log('toggle_wall', data);
        categ.is_active = action;
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

     
    $scope.addTopCateg = function(types){
      if(types==1){
        $('[data-type="addcolumn"]').hide();
        $('[data-type="addcateg"]').show();
      }
      else{
        $('[data-type="addcateg"]').hide();
        $('[data-type="addcolumn"]').show();
      }
      console.log('column addTopCateg');
        $('.column.column-add-column').removeClass('column-add-frommini-active');
        $('.column-panel-list.word').removeClass('active');


        $('.column.column-add-column').toggleClass('active');
          var w = '-405px'
          if($('.column.column-add-column').hasClass('active')){
            w = '0px'
          }

        if($('.wall-add-column').hasClass('actives')){
        

          

        }
        else{
          
          $('.column.column-add-column').animate(
                  {'margin-left': w}, {
                  easing: null,
                  duration: 1500,
                  complete: function() {
                  }
          });
        }
    };

    /*Создание категории*/
    $scope.sendFormAddTopCateg = function($event){
      console.log("sendFormAddTopCateg", $event);
      console.log('form');
      var t = $($event.currentTarget);
      var parent = $(t).parents('.cc-add-column');
      var column_parent = $(t).parents('.column.column-add-column');
      var data = {};
      var name = parent.find('.input-add-column').val().toUpperCase();
      if(name.length==0){
        toastr.warning('Not name');
        return false
      }
      else if(name.length>25){
        toastr.warning('Max length');
        return false
      }
      
      Wall.create_categ_top(name).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
          console.log('create_categ_top', data); 
          $scope.categories.push(data.data);
          $('.menu-item-adm-add-categ').click()
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    $scope.sendFormAddColumn = function($event, type){
      console.log("sendFormAddColumn", $event, type);
      console.log('form');
      var t = $($event.currentTarget);
      var parent = $(t).parents('.cc-add-column');
      var column_parent = $(t).parents('.column.column-add-column');
      var data = {};
      var name = parent.find('.input-add-column').val();
      if(name.length==0){
        toastr.warning('Not name');
        return false
      }
      else if(name.length>25){
        toastr.warning('Max length');
        return false
      }
      

      
        
          var sources = [];
          var matchings = [];
          var excludings = [];
          var walls = [];
          $('.add-source-group-block.matching .add-source-group-block-word > span').each(function(indx, element){
             matchings.push($(element).html());
          });
          $('.add-source-group-block.excluding .add-source-group-block-word > span').each(function(indx, element){
             excludings.push($(element).html());
          });
          
          data['name'] = name;
          data['wall_id'] = $('.nav-menu-item-top.active').data('id');
          data['icon'] = ' ';
          data['color'] = parent.find('.menu-item-img-word-btn').data('color');
          data['sources'] = sources;
          data['matchings'] = matchings;
          data['excludings'] = excludings;
          data['size'] = parseInt(parent.find('.acs-show-group-span.acs-size.acs-size-active').data('size'));
          data['types'] = 'collection';
          data['link'] = 'collection';

          console.log("DATA", data);
          Column.create_top(data).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
          function ColumnAddSuccessFn2(data, status, headers, config) {
            console.log('create_trend', data); 
            $window.location.reload();
            
          }
          function ColumnAddErrorFn2(data, status, headers, config) {
          console.log(data, status, headers, config);
          }

        

        console.log('2', data);


    };
     

    $scope.settingsEditWordsWColumn = function($event, action){
      var element = $($event.currentTarget);
      var parent = element.parents('.add-source-group-block');
      var word_parents = parent.find('.add-words-group-block');
      var column_parents = element.parents('.column');
      var column_id = column_parents.data('column');
      console.log('settingsEditWordsWColumn', column_parents);
      var next = false,
          actions = 'add',
          types = 'matching',
          words = [];
      if(action == 'delete'){
        console.log('del', parent);
        parent.find('input').val('').keyup();
        element.parent().remove();
        next = true;
        actions = 'delete';
      }
      else if(action == 'add'){
        console.log('add', parent);
        parent.find('.add-source-group-block-word.dop').removeClass('acs-load').removeClass('dop');
        parent.find('input').val('').keyup();
        next = true;
      }

      if(!parent.hasClass('matching')){
        types = 'excluding'
      }

      word_parents.find(".add-source-group-block-word").each(function(indx, element){
        words.push($(element).find('span').html().toUpperCase())
      });

      
      if(!column_parents.hasClass('column-add-column') && next){

        Column.settings_word(column_id, words, types, actions).then(fnSuccessFn, fnErrorFn);
        //toastr.error('sdsd34234242342hd');

        function fnSuccessFn(data, status, headers, config) {
          console.log('settings_word', data);
        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      }
    }; 

    $scope.ShowProfile = function($event){
      $location.path('/@'+ $scope.user.username)
    };

    $scope.showColumnSettings = function(el){
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if(!$header.hasClass('cntr-column-header-open-set')){
        $header.addClass('cntr-column-header-open-set');
        $timeout(function () {
          $header.addClass('cntr-column-header-open-set-vis');
        }, 200);
      }
      else{
        $header.removeClass('cntr-column-header-open-set-vis');
        $timeout(function () {
          $header.removeClass('cntr-column-header-open-set');
        }, 300);
      }
    };

    $scope.toggleTopColumn = function(column){
      console.log("toggleTopColumn", column);

      var action = true;
      if(column.is_active){
        action = false
      }

      Column.toggle_top(action, column.id).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
        console.log('create_trend', data);
        column.is_active = action;
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    $scope.nameTopColumn = function($event, column){
      console.log("nameTopColumn", column);
      var el = $($event.currentTarget),
          prt = el.parents('.table-settings-row'),
          name = prt.find('.input-name-column').val();

      Column.name_top(name, column.id).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
        console.log('name_top', data);
        column.name = name;
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };



  }]);
