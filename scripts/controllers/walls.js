'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:WallsCtrl
 * @description
 * # WallsCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('WallsCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.removeClass('open-profile');

    if($scope.params.category){
      $scope.cat = $scope.params.category.toUpperCase();
    }
    else{
      $scope.cat = 'RECOMMENDED';
    }
    


    $scope.userType = 'Anonymus';

    $scope.top = true;
    $scope.cat_id = null;

    $scope.walls = true;

    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params);

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          $scope.slugusername = $scope.user.username;
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();



    var vm = this;


    $scope.posts = [];

    $scope.columns = [];

    $scope.profiles = [];

    $scope.categories = [];

    $scope.walls = [];
    $scope.wallss = [];
    //$scope.walls = [{'id': 1, 'name': 'one', 'icon': 'icon_w_17.png', 'current': true}, {'id': 2, 'name': 'two', 'icon': 'icon_w_17.png', 'current': false}, {'id': 3, 'name': 'three', 'icon': 'icon_w_17.png', 'current': false}];

    activate();

    function get_columns() {

      Wall.walls($scope.cat, $scope.cat_id).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('get_columnsget_columns', data);
          console.log(data.data.columns);

          $scope.userType = data.data.userType;
          $scope.columns = data.data.columns;
          $scope.walls = data.data.walls;
          $scope.wallss = data.data.wallss;
      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    
    

    function activate() {

      Wall.category('is_collections').then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('categorycategory', data);
          $scope.categories = data.data.categories;

          for(var i = 0; i < $scope.categories.length; i++){
            console.log('.$scope.categories', $scope.categories[i])
            if($scope.categories[i].name == $scope.cat){
              $scope.cat_id = $scope.categories[i].id
            }
          }
          if(($scope.params.category && $scope.cat_id) || !$scope.params.category){
            get_columns();
          }
          else{
            console.log('nononononono')
          }
          

      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };


    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

    };

    /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);

      if(!e.hasClass('active-menu-wl')){
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        $scope.$apply();
      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.topWall(wall)
      }
      
    };

    $scope.wall_image = null;

    $scope.PictureWallSettings = function($event){
      var src = URL.createObjectURL($event.target.files[0]);
      var image = $event.target.files[0];
      console.log('settingsPictureProfile image', image.size);

      $scope.wall_image = $event.target.files[0];

      $('.sw-wall').css({'background-image': 'url('+src+')'})

    };  

    $scope.switchWallCat = function($event, cat){
      var el = $($event.currentTarget),
          prt = el.parent(),
          name = el.data('name'),
          url = "/walls"
      
      if(el.hasClass('active')){
        return false
      }

      prt.find('.nav-menu-item-top').removeClass('active');
      el.addClass('active');

      if(name!='RECOMMENDED'){
        url += "/" + name.toLowerCase()
      }

      $location.path(url);
    };

    $scope.ShowProfile = function($event){
      $location.path('/@'+ $scope.user.username)
    };

    $scope.addWallCateg = function(types){
      if(types==1){
        $('[data-type="addcolumn"]').hide();
        $('[data-type="addcateg"]').show();
      }
      else{
        $('[data-type="addcateg"]').hide();
        $('[data-type="addcolumn"]').show();
        var block = $('.block-collections'),
            wall = $('.sw-wall');

        /*title wall*/
        block.find('.admin-sw-name').unbind().keyup(function(e){
          var el = $(e.currentTarget),
              value = el.val(),
              h2 = el.siblings('h2');
          h2.html(value);
        });

        /*desc wall*/
        block.find('.admin-sw-desc').unbind().keyup(function(e){
          var el = $(e.currentTarget),
              value = el.val(),
              h2 = el.siblings('h2');
          h2.html(value);
        });

        /*color wall*/
       /* block.find('.admin-sw-color').unbind().click(function(e){
          var el = $(e.currentTarget);
          console.log('c', el)
        });*/
        block.find('.admin-sw-color').colorpicker().on('changeColor', function (e) {
          var color = e.color.toHex();
          console.log('color', color);
          wall.css('background-color', color).data('color', color)
        });

        /*color image*/
        block.find('#wallimage').change(function(event){
          $scope.PictureWallSettings(event);
        });

        /** add column */
        block.find('.btn-add-column-wall').unbind().click(function(e){
          var el = $(e.currentTarget);
          el.toggleClass('active');
          if(el.hasClass('active')){
            $('.column-add-column-wall').show()
          }
          else{
            $('.column-add-column-wall').hide()
          }
        });

        /* add wall save */
        block.find('.btn-add-column-save').unbind().click(function(e){
          $scope.addSaveWall(e);
        });


      }
      console.log('column addWallCateg');
        $('.column.column-add-column').removeClass('column-add-frommini-active');
        $('.column-panel-list.word').removeClass('active');


        $('.column.column-add-column').toggleClass('active');
          var w = '-405px'
          if($('.column.column-add-column').hasClass('active')){
            w = '0px'
          }

        if($('.wall-add-column').hasClass('actives')){
        

          

        }
        else{
          
          $('.column.column-add-column').animate(
                  {'margin-left': w}, {
                  easing: null,
                  duration: 1500,
                  complete: function() {
                  }
          });
        }
    };

    /*save wall*/
    $scope.addSaveWall = function($event){
      console.log("addSaveWall", $event);
      var data = {},
          name = $('.admin-sw-name').val(),
          desc = $('.admin-sw-desc ').val(),
          columns = [],
          color = $('.sw-wall-edit').data('color'),
          cat_id = $('.nav-menu-item-rec-walls.active').data('id');

      $(".lrw-wall-column-edit").each(function(indx, element){
        columns.push($(element).data('id'));
      });

      if(name.length<=0){
        toastr.warning('Not name')
      }
      else if(desc.length<=0){
        toastr.warning('Not description')
      }
      else if(columns.length<=0){
        toastr.warning('Not columns')
      }
      else{
        data['color'] = color;
        data['name'] = name;
        data['desc'] = desc;
        data['columns'] = columns;
        data['cat_id'] = cat_id;
        if($scope.wall_image){
          data['image'] = $scope.wall_image; 
        }
        

        Wall.create_wall(data).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
        function ColumnAddSuccessFn2(data, status, headers, config) {
            console.log('create_wall', data); 

            $scope.wall_image = null;
        }
        function ColumnAddErrorFn2(data, status, headers, config) {
          console.log(data, status, headers, config);
        }
      }
    };

    /*Создание категории*/
    $scope.sendFormAddWallsCateg = function($event){
      console.log("sendFormAddWallsCateg", $event);
      console.log('form');
      var t = $($event.currentTarget);
      var parent = $(t).parents('.cc-add-column');
      var column_parent = $(t).parents('.column.column-add-column');
      var data = {};
      var name = parent.find('.input-add-column').val().toUpperCase();
      if(name.length==0){
        toastr.warning('Not name');
        return false
      }
      else if(name.length>25){
        toastr.warning('Max length');
        return false
      }
      
      Wall.create_categ_walls(name).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
          console.log('create_categ_walls', data); 
          $scope.categories.push(data.data);
          $('.menu-item-adm-add-categ').click()
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    $scope.toggleWallsColumn = function(categ){
      console.log("toggleWallsColumn", categ);

      var action = true;
      if(categ.is_active){
        action = false
      }

      Wall.toggle_cat_walls(action, categ.id).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
      function ColumnAddSuccessFn2(data, status, headers, config) {
        console.log('toggle_cat_walls', data);
        categ.is_active = action;
      }
      function ColumnAddErrorFn2(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    /** Меняем тип колонки при создании*/
    $scope.ChangeTypeAddColumn = function($event){
        console.log('ChangeTypeAddColumn', $event);
        var el = $($event.currentTarget),
            standart = $('[data-type="addstandart"]'),
            custom = $('[data-type="addcustom"]');

        standart.hide();
        custom.show();
    };

    $scope.AddColumnCustomClose = function($event){
        console.log('AddColumnCustomClose', $event);
        var el = $($event.currentTarget),
            standart = $('[data-type="addstandart"]'),
            custom = $('[data-type="addcustom"]');

        custom.hide();
        standart.show();
        
    };


    $scope.AddColumnWords = function($event){
        console.log('AddColumnWords');
        var el = $($event.currentTarget),
            letter = el.data('letter'),
            prt = el.parents('.addcolumn-block-words'),
            feeds = $('.addcolumn-block-feeds'),
            sites = new Array(),
            types = 'fromletter';

        //prt.hide();
        prt.addClass('slidehidden');
        
        $scope.addcolumns = [];

        function groupBy( array , f ){
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        };

        var feed = '<add-column-html a-show-rss-feeds="AShowRssFeeds" add-column-from-list="addColumnFromList" add-column-back-feeds="AddColumnBackFeeds" letter="letter" addcolumnsres="addcolumnsres" addcolumns="addcolumns"></add-column-html>';

        Column.get_letters(letter, types).then(ColumnSuccessFn, ColumnErrorFn);

        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_letters', data.data.data);
          var d = data.data.data;
          var res = data.data.failure ? false : true;
          for(var i = 0; i < d.length; i++){
            var sh = d[i].short_url;
            var s = {'short_url': sh, 'data': d[i]};
            sites.push(s);
          }

          var result = groupBy(sites, function(item){return [item.short_url];});

          $scope.addcolumns = result;
          $scope.addcolumnsres = res;
          $scope.letter = letter;

          feeds.html($compile(feed)($scope));
          
          //feeds.show();                                     
        }


        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 

        feeds.addClass('slideopened');
                
    };

    /**
    * из инпута ввода
    */
    $scope.AddInColumnSearchWords = function($event){
        console.log('AddInColumnSearchWords', $event);
        var el = $($event.currentTarget),
            prt = el.parents('.addcolumn-header-standart'),
            val = prt.find('.input-add-column').val();

        if(val.length>0){
          $scope.AddColumnSearchWords(val);
        }      
    };
    $scope.AddColumnSearchWords = function(value){
        console.log('AddColumnSearchWords', value);

        var letter = value,
            prt = $('.addcolumn-block-words'),
            feeds = $('.addcolumn-block-feeds'),
            sites = new Array(),
            types = 'fromsearch';

        prt.addClass('slidehidden');

        if(letter.length<=0){
          feeds.find('.addcolumn-block-feed-back').click();
          return false
        }
        
        $scope.addcolumns = [];

        function groupBy( array , f ){
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        };

        var feed = '<add-column-html a-show-rss-feeds="AShowRssFeeds" add-column-back-feeds="AddColumnBackFeeds" letter="letter" addcolumnsres="addcolumnsres" addcolumns="addcolumns"></add-column-html>';

        Column.get_letters(letter, types).then(ColumnSuccessFn, ColumnErrorFn);

        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_letters', data.data.data);
          var d = data.data.data;
          var res = data.data.failure ? false : true;
          for(var i = 0; i < d.length; i++){
            var sh = d[i].short_url;
            var s = {'short_url': sh, 'data': d[i]};
            sites.push(s);
          }

          var result = groupBy(sites, function(item){return [item.short_url];});

          $scope.addcolumns = result;
          $scope.addcolumnsres = res;
          $scope.letter = letter;

          feeds.html($compile(feed)($scope));
                                              
        }

        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 

        feeds.addClass('slideopened');
                
    };

    $scope.AddColumnBackFeeds = function($event){
        console.log('AddCOlumnBackFeeds');
        var el = $($event.currentTarget),
            prt = $('.addcolumn-block-feeds'),
            words = $('.addcolumn-block-words');

        prt.removeClass('slideopened');
        words.removeClass('slidehidden');
        
    };

    $scope.AShowRssFeeds = function($event){
        console.log('AShowRssFeeds');
        var el = $($event.currentTarget),
            pallet = el.parents('.addcolumn-block-pallet'),
            txt = el.find('span'),//border-color:  transparent transparent #cc0000 transparent;
            color = pallet.data('color');

        if(pallet.hasClass('mini')){
          pallet.removeClass('mini').addClass('full');
          txt.html('HIDE RSS FEEDS');
          el.find("i").css({"border-color": "transparent transparent "+color+" transparent"});
          pallet.find('.media-list-body-dots').css({"background": color});
          pallet.find('.mini-column-add').css({"border-color": color, "color": color});
        }
        else{
          pallet.removeClass('full').addClass('mini');
          txt.html('SHOW RSS FEEDS');
          el.find("i").css({"border-color": ""+color+" transparent transparent  transparent"})
        }  
    };

    $scope.addColumnFromList = function(id){
      console.log('addColumnFromList', id);
      var item = $('.addcolumn-block-pallet-item[data-id="'+id+'"]'),
          prt = item.parents('.addcolumn-block-pallet'),
          favicon = prt.find('.addcolumn-block-pallet-favicon').attr('src');

      var h = '<div data-id="'+id+'" class="lrw-wall-column lrw-wall-column-edit"><img class="" src="'+favicon+'"></div>';

      $('.sw-wall-columns').prepend(h);

      $('.lrw-wall-column-edit').unbind().click(function(e){
          var el = $(e.currentTarget);
          el.remove();
          
        });
    };
     



  }]);
