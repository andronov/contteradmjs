'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:SitesCtrl
 * @description
 * # SitesCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('SitesCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;

    $scope.short_url = $scope.params.domain;

    $scope.userType = 'Anonymus';

    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params);

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();



    var vm = this;


    $scope.posts = [];

    $scope.columns = [];

    $scope.profiles = [];

    $scope.walls = [];
    //$scope.walls = [{'id': 1, 'name': 'one', 'icon': 'icon_w_17.png', 'current': true}, {'id': 2, 'name': 'two', 'icon': 'icon_w_17.png', 'current': false}, {'id': 3, 'name': 'three', 'icon': 'icon_w_17.png', 'current': false}];

    activate();


    //vm.columns = [{'column': {'id': 25, 'name': 'bbc'}, 'data': [{'id': 1, 'title': 'test1'}, {'id': 2, 'title': 'test2'}]}, {'column': {'id': 28, 'name': 'cnn'}, 'data': [{'id': 3, 'title': 'test3'}]}];

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf
     */
    function activate() {

      Wall.sites($scope.short_url).then(postsSuccessFn, postsErrorFn);

      /*$scope.$on('post.created', function (event, post) {
        vm.posts.unshift(post);
      });

      $scope.$on('post.created.error', function () {
        vm.posts.shift();
      });*/


      /**
       * @name postsSuccessFn
       * @desc Update thoughts array on view
       */
      function postsSuccessFn(data, status, headers, config) {
        console.log('short_url', data.data);
        $scope.userType = data.data.userType;
        $scope.columns = data.data.columns;
        $scope.walls = data.data.walls;
        //$scope.posts = data.data;

      }


      /**
       * @name postsErrorFn
       * @desc Show snackbar with error
       */
      function postsErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        //Snackbar.error(data.error);
      }
    }


    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

    };
     



  }]);
