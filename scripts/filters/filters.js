'use strict';

/**
 * @ngdoc filters
 * @name contterApp.filters:feed
 * @description
 * # feed
 */
function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth*ratio, height: srcHeight*ratio };
};

angular.module('contterApp')
  .filter('noti', function() {
    return function(value, column_id) {
         console.log('value', value, column_id)
        return value;
    };
  }).filter('firstletter', function() {
    return function(value) {
        var reg = /\w/;
        return value.match(reg);
    };
  }).filter('firstletterword', function() {
    return function(value) {
        var reg = /\w/;
        if(value != undefined){return value.match(reg);}
        else{return 'N'}
        
    };
  }).filter('randomcolorcolumnw', function() {
    return function(value) {
      var colors = ['#2ecc71', '#3498db', '#9b59b6', '#2c3e50', '#e67e22', '#c0392b', '#f1c40f'];
      var item = colors[Math.floor(Math.random()*colors.length)];
      if(value!= undefined && value.length >= 1){
        return item;
      }
      else{
        return '#c0d3ef'
      }
      
    };
  }).filter('imagesizeitem', function() {
    return function(value, size) {      
      if(value){
        var w = value.medium_img.width;
        var h = value.medium_img.height;
        if(size == 300){
          size = 320
        }
        else if(size == 500){
          size = 480
        }
        var res = calculateAspectRatioFit(w, h, size, 10000);
        return 'width: ' + res.width + 'px; height: ' + res.height + 'px;'
      }
      return 'width: 450px;height: 250px;'
      
    };
  }).filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

